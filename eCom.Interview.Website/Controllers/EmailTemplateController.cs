﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using eCom.Interview.Data.Models;
using eCom.Interview.Website.Internal.AppSettings;
using eCom.Interview.Website.Internal.Enums;
using eComEngine.Interview.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace eCom.Interview.Website.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmailTemplateController : Controller
    {
        private readonly IRepository<EmailTemplate> _emailTemplateRepository;

        public EmailTemplateController(IOptions<EmailTemplateRepositorySettings> options)
        {
            _emailTemplateRepository = new EmailTemplateXmlFileRepository(options.Value.XmlFilePath);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IEnumerable<EmailTemplate> Get(string filter, EmailTemplateSortOrder sortColumn, bool sortDescending)
        {
            Expression<Func<EmailTemplate, bool>> filterFunction = t => true;
            Expression<Func<IQueryable<EmailTemplate>, IQueryable<EmailTemplate>>> sortColumnFunction = t => t.OrderBy(t => t.DateUpdated);

            if (!string.IsNullOrEmpty(filter) && !string.IsNullOrWhiteSpace(filter))
            {
                filterFunction = t => t.FromAddress.Contains(filter, StringComparison.CurrentCultureIgnoreCase) || t.EmailLabel.Contains(filter, StringComparison.CurrentCultureIgnoreCase);
            }

            switch (sortColumn)
            {
                case EmailTemplateSortOrder.EmailLabel:
                {
                    if (sortDescending)
                    {
                         sortColumnFunction = t => t.OrderByDescending(t => t.EmailLabel);
                    }
                    else
                    {
                         sortColumnFunction = t => t.OrderBy(t => t.EmailLabel);
                    }
                    break;
                }
                case EmailTemplateSortOrder.FromAddress:
                { 
                    if (sortDescending)
                    {
                         sortColumnFunction = t => t.OrderByDescending(t => t.FromAddress);
                    }
                    else
                    {
                         sortColumnFunction = t => t.OrderBy(t => t.FromAddress);
                    }
                    break;
                }
                case EmailTemplateSortOrder.DateUpdated:
                default:
                {
                    if (sortDescending)
                    {
                         sortColumnFunction = t => t.OrderByDescending(t => t.DateUpdated);
                    }
                    else
                    {
                         sortColumnFunction = t => t.OrderBy(t => t.DateUpdated);
                    }
                    break;
                }
            }

            var hi = _emailTemplateRepository.Where(filterFunction, sortColumnFunction).Result;
            return _emailTemplateRepository.Where(filterFunction, sortColumnFunction).Result;
        }
    }
}
