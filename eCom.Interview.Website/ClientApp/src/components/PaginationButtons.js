﻿import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft} from '@fortawesome/free-solid-svg-icons'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'
import { faAngleDoubleLeft } from '@fortawesome/free-solid-svg-icons'
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'

export class PaginationButtons extends Component {

    constructor(props) {
        super(props);
        this.firstPage = this.firstPage.bind(this);
        this.prevPage = this.prevPage.bind(this);
        this.nextPage = this.nextPage.bind(this);
        this.lastPage = this.lastPage.bind(this);
    }

    firstPage() {
        this.props.onChange(1);
    }

    prevPage() {
        this.props.onChange(this.props.currentPage - 1);
    }

    nextPage() {
        this.props.onChange(this.props.currentPage + 1);
    }

    lastPage() {
        this.props.onChange(Math.ceil(this.props.totalPages));
    }

    render() {
        return (
            <div className='page-container'>
                <button disabled={this.props.currentPage <= 1} className='page-button' onClick={() => this.firstPage()}>
                    <FontAwesomeIcon icon={faAngleDoubleLeft} />
                </button>

                <button disabled={this.props.currentPage <= 1} className='page-button' onClick={() => this.prevPage()}>
                    <FontAwesomeIcon icon={faAngleLeft} />
                </button>

                <div className='page-display'>{this.props.currentPage}</div>

                <button disabled={this.props.currentPage > this.props.totalPages} className='page-button' onClick={() => this.nextPage()}>
                    <FontAwesomeIcon icon={faAngleRight} />
                </button>

                <button disabled={this.props.currentPage > this.props.totalPages} className='page-button' onClick={() => this.lastPage()}>
                    <FontAwesomeIcon icon={faAngleDoubleRight} />
                </button>
            </div>
        );
    }
}