﻿import React, { Component } from 'react';
import { SearchBar } from './SearchBar';
import { PaginationButtons } from './PaginationButtons';
import { SortingButtons } from './SortingButtons';
import { EmailTemplateList } from './EmailTemplateList';

export class DisplayList extends Component {

    constructor(props) {
        super(props);
        this.setFilter = this.setFilter.bind(this);
        this.setPage = this.setPage.bind(this);
        this.setSort = this.setSort.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.state = {
            data: [],
            loading: true,
            filter: '',
            pageLimit: 10,
            currentPage: 1,
            totalPages: 1,
            sortColumn: 3,
            sortDescending: true,
        };
    }

    componentDidMount() {
        this.fetchData(this.state.filter, this.state.sortColumn, this.state.sortDescending);
    }

    setFilter(filter) {
        this.setState({ filter: filter, currentPage: 1 });
        this.fetchData(filter, this.state.sortColumn, this.state.sortDescending);
    }

    setPage(currentPage) {
        this.setState({ currentPage: currentPage});
    }

    setSort(sortColumn, sortDescending) {
        this.setState({ sortColumn: sortColumn, sortDescending: sortDescending, currentPage: 1 });
        this.fetchData(this.state.filter, sortColumn, sortDescending);
    }

    render() {
        const startPageIndex = (this.state.currentPage - 1) * this.state.pageLimit;
        const endPageIndex = startPageIndex + this.state.pageLimit;

        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : <EmailTemplateList pageLimit={this.state.pageLimit} currentPage={this.state.currentPage} templates={this.state.data.slice(startPageIndex, endPageIndex)} />;

        return (
            <div>
                <div className='page-sort-container'>
                    <SearchBar onChange={this.setFilter} />
                    <PaginationButtons currentPage={this.state.currentPage} totalPages={this.state.totalPages} onChange={this.setPage} />
                </div>
                <SortingButtons sortColumn={this.state.sortColumn} sortDescending={this.state.sortDescending} onChange={this.setSort} />
                <div>
                    {contents}
                </div>

            </div>
        );
    }

    fetchData(filter, sortColumn, sortDescending) {
        this.setState({ loading: true, selectedPageData: new Set() });
        fetch(`emailtemplate?filter=${filter}&sortColumn=${sortColumn}&sortDescending=${sortDescending}`)
            .then((response) => response.json())
            .then((data) => {
                this.setState({ data: data, loading: false, totalPages: data.length / this.state.pageLimit });
            });
    }
}