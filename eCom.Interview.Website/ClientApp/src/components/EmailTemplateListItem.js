﻿import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'

export class EmailTemplateListItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isNestedDataDisabled: true
        }
    }

    render() {
        return (
            <div className='list-item'>
                <button className='list-item-drop-button' onClick={() => this.setState({ isNestedDataDisabled: !this.state.isNestedDataDisabled })}>
                    <div className='list-cell-container'>
                        <div className='list-cell'>{this.props.template.emailLabel}</div>
                        <div className='list-cell'>{this.props.template.fromAddress}</div>
                        <div className='list-cell-with-icon'>{this.props.template.dateUpdated}
                            <div className='list-item-drop-button-icon'><FontAwesomeIcon icon={this.state.isNestedDataDisabled ? faAngleRight : faAngleDown} /></div>
                        </div>
                    </div>
                </button>
                {!this.state.isNestedDataDisabled &&
                    <div className='nested-info-container'>
                        <div className='nested-info-label'>VERSION EMAIL LABELS</div>
                        <div className='nested-info-data-container'>
                        {this.props.template.versions.map(version => <div key={version.id} className='list-item-nested-info'>{version.emailLabel}</div>)}
                        </div>
                    </div>
                }
            </div>
        );
    }
}