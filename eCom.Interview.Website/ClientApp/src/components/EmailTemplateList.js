﻿import React, { Component } from 'react';
import { EmailTemplateListItem } from './EmailTemplateListItem';


export class EmailTemplateList extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='list-container'>
                {this.props.templates.map(template =>
                    <EmailTemplateListItem key={template.id} template={template} />
                )}
            </div>
        );
    }
}