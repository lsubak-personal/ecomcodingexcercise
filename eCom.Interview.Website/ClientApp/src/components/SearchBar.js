﻿import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'

export class SearchBar extends Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.onChange(e.target.value);
    }

    render() {
        return (
            <div className='search-bar-container'>
                <div className='search-bar-icon-container'>
                    <FontAwesomeIcon className='search-bar-icon' icon={faSearch} />
                </div>
                <input className='search-bar' type='text' onChange={this.handleChange} />
            </div>
        );
    }
}