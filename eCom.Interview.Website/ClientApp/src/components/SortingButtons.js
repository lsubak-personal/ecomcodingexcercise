﻿import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSort } from '@fortawesome/free-solid-svg-icons'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'
import { faCaretUp } from '@fortawesome/free-solid-svg-icons'

export class SortingButtons extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.getSortIcon = this.getSortIcon.bind(this);
    }

    handleClick(e, sortColumn) {
        if (sortColumn === this.props.sortColumn) {
            this.props.onChange(sortColumn, !this.props.sortDescending);
        }
        else {
            this.props.onChange(sortColumn, true);
        }
    }

    getSortIcon(sortColumn) {
        if (sortColumn === this.props.sortColumn) {
            if (this.props.sortDescending) {
                return faCaretDown;
            }
            else {
                return faCaretUp;
            }
        }
        else {
            return faSort;
        }
    }

    render() {
        return (
            <div className='header-container'>
                {['EMAIL LABEL', 'FROM ADDRESS', 'DATE UPDATED'].map((header, index) =>
                    <button key={header} className='header-button' onClick={() => this.handleClick(this, index + 1)}>
                        <div className='header-sort-label'>{header}</div>
                        <div className='header-sort-icon-container'>
                            <FontAwesomeIcon className='header-sort-icon' icon={this.getSortIcon(index + 1)} />
                        </div>
                    </button>
                )}
            </div>
        );
    }
}