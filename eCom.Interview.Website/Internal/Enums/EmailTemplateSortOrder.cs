﻿namespace eCom.Interview.Website.Internal.Enums
{
    public enum EmailTemplateSortOrder
    {
        EmailLabel = 1,
        FromAddress = 2,
        DateUpdated = 3
    }
}
